# Project name 
Mapare Sintactica - NP

# Description
Crearea unui program capabil să găsească grupurile nominale în arborele sintactic FDG al unei fraze, să antreneze un NP­parser capabil să primească în intrare șirul de cuvinte al unui NP și să genereze miniarborele FDG corespunzător.

# Members  
- Cosmin Florean
- Alexandru Cires