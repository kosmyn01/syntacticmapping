import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import treeModule.*;
import treeModule.exceptions.*;

public class AlexMainClass {

	List<SyntaxWord> xmlWordList;
	SyntaxSentence xmlSentence;
	TreeInterface tree;

	public AlexMainClass() {
		loadHardcodedData();
	}

	public static void main(String[] args) {
		AlexMainClass alex = new AlexMainClass();
		// alex.simpleTest();
		alex.xmlTest();
	}

	public void loadHardcodedData() {
		xmlSentence = new SyntaxSentence("11_citate1_977_", "Radu&apos;s parser", "Ugla", "2015-15-20");
		SyntaxWord word1 = new SyntaxWord("1", "�nt�mpl�rile", "�nt�mplare", "Ncfpry", "9", "", "sbj.");
		SyntaxWord word2 = new SyntaxWord("2", "din", "din", "Sp", "1", "", "a.subst.");
		SyntaxWord word3 = new SyntaxWord("3", "Cluj", "Cluj", "Np", "2", "", "prep.");
		SyntaxWord word4 = new SyntaxWord("4", "trebuie", "trebui", "Vmip3s", "0", "", "");
		SyntaxWord word5 = new SyntaxWord("5", "s�", "s�", "Qs", "4", "", "sbj.");
		SyntaxWord word6 = new SyntaxWord("6", "fi", "fi", "Van", "7", "", "aux.");
		SyntaxWord word7 = new SyntaxWord("7", "fost", "fi", "Vap", "9", "", "aux.");
		SyntaxWord word8 = new SyntaxWord("8", "adeseori", "adeseori", "Rg", "9", "", "c.c.m.");
		SyntaxWord word9 = new SyntaxWord("9", "fr�m�ntate", "fr�m�ntat", "Afpfprn", "5", "", "subord.");
		SyntaxWord word10 = new SyntaxWord("10", "cu", "cu", "Sp", "9", "", "c.c.t.");
		SyntaxWord word11 = new SyntaxWord("11", "prilejul", "prilej", "Ncmsry", "10", "", "prep.");
		SyntaxWord word12 = new SyntaxWord("12", "�nt�lnirilor", "�nt�lnire", "Ncfpoy", "11", "", "a.subst.");
		SyntaxWord word13 = new SyntaxWord("13", "care", "care", "Pw3--r", "12", "", "a.vb.");
		SyntaxWord word14 = new SyntaxWord("14", "aveau loc", "avea_loc", "Vmii3p", "13", "", "subord.");
		SyntaxWord word15 = new SyntaxWord("15", "�n", "�n", "Sp", "14", "", "c.c.t.");
		SyntaxWord word16 = new SyntaxWord("16", "fiecare", "fiecare", "Pi3-sr", "17", "", "a.adj.");
		SyntaxWord word17 = new SyntaxWord("17", "var�", "var�", "Ncfsrn", "15", "", "prep.");
		SyntaxWord word18 = new SyntaxWord("18", ".", ".", "PERIOD", "4", "", "punct.");
		xmlWordList = new ArrayList<>();
		xmlWordList.add(word1);
		xmlWordList.add(word2);
		xmlWordList.add(word3);
		xmlWordList.add(word4);
		xmlWordList.add(word5);
		xmlWordList.add(word6);
		xmlWordList.add(word7);
		xmlWordList.add(word8);
		xmlWordList.add(word9);
		xmlWordList.add(word10);
		xmlWordList.add(word11);
		xmlWordList.add(word12);
		xmlWordList.add(word13);
		xmlWordList.add(word14);
		xmlWordList.add(word15);
		xmlWordList.add(word16);
		xmlWordList.add(word17);
		xmlWordList.add(word18);
	}

	public void simpleTest() {
		NodeInterface node1 = new SyntaxNode("First");
		NodeInterface node2 = new SyntaxNode("Second");
		NodeInterface node3 = new SyntaxNode("Third");
		NodeInterface node4 = new SyntaxNode("Fourth");
		NodeInterface node5 = new SyntaxNode("Fifth");

		tree = new SyntaxTree();
		try {
			tree.addNode(tree.getRootNode(), node1);
			tree.addNode(node1, node2);
			tree.addNode(node1, node3);
			tree.addNode(node2, node4);
			tree.addNode(node2, node5);
			System.out.println(tree);

			// List<NodeInterface> leaves = tree.getLeafNodesForNode(node2);
			// System.out.println("Leaf nodes for node 2:");
			// for (NodeInterface leaf : leaves) {
			// System.out.print(leaf + " ");
			// }
			// System.out.println();

			// node1.removeDescendants();
			// List<NodeInterface> descendants =
			// tree.getRootNode().getDescendants();
			// System.out.println("\nDescendants root:");
			// for(NodeInterface descendant: descendants) {
			// System.out.println(descendant);
			// }
			// descendants = node2.getDescendants();
			// System.out.println("\nDescendants node 2:");
			// for(NodeInterface descendant: descendants) {
			// System.out.println(descendant);
			// }

			// tree.removeNode(node2);
			// System.out.println("\nNew Tree: \n" + tree);

			//// Merging test
			System.out.println("Merging node2 with node4");
			try {
				tree.mergeNodes(node2.getNodeID(), node4.getNodeID());
				System.out.println("Done merging");
			} catch (InvalidIDFormatException e) {
				e.printStackTrace();
			}
			System.out.println("\n" + tree);

		} catch (NodeNotFoundException | ForbiddenTreeStructureException e) {
			e.printStackTrace();
		}

		System.out.println("\n\n");
		// TESTING CHUNK ADDING AND READING

	}

	@SuppressWarnings("unchecked")
	public void xmlTest() {
		try {
			String pathForChunkerXML = "../output_NpChunkerRo.xml";
			File inputFile = new File("../sentenceTest.xml");
			SAXReader reader = new SAXReader();
			Document document;
			document = reader.read(inputFile);
			SyntaxSentence sentence;
			SyntaxWord word;
			List<SyntaxWord> wordList;
			List<Node> nodes = document.selectNodes("/treebank/sentence");
			for (Node node : nodes) {
				sentence = new SyntaxSentence(node.valueOf("@id"), node.valueOf("@parser"), node.valueOf("@user"),
						node.valueOf("@date"));
				List<Node> nodes2 = node.selectNodes("word");
				wordList = new ArrayList<>();
				for (Node node2 : nodes2) {
					word = new SyntaxWord(node2.valueOf("@id"), node2.valueOf("@form"), node2.valueOf("@lemma"),
							node2.valueOf("@postag"), node2.valueOf("@head"), node2.valueOf("@chunk"),
							node2.valueOf("@deprel"));
					wordList.add(word);
				}
				tree = new SyntaxTree(sentence, wordList);
				System.out.println("Generated tree from xml:\n\n" + tree);
				System.out.println("Loading NPChuncker service results...");
				tree.addNPChunks(pathForChunkerXML);
				System.out.println("Result loaded:\n" + tree.getNPChunks());
				tree.getAllNodesInsideNPChunks();
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (NodeNotFoundException e) {
			e.printStackTrace();
		}
	}

}
