import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.xml.rpc.ServiceException;

import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.TreeLayout;
import org.abego.treelayout.util.DefaultConfiguration;
import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import treeModule.SyntaxSentence;
import treeModule.SyntaxTree;
import treeModule.SyntaxWord;
import treeModule.exceptions.NodeNotFoundException;
import uaic.webFdgRo.FdgParserRoWS_Service;
import uaic.webFdgRo.FdgParserRoWS_ServiceLocator;

import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;

public class GUI {

	private JFrame frmSynta;
	private JTextField senteceInput;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmSynta.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSynta = new JFrame();
		frmSynta.setTitle("SyntacticMapping");
		frmSynta.setBounds(100, 100, 675, 497);
		frmSynta.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmSynta.getContentPane().setLayout(null);

		GraphProj graphProj = new GraphProj();
		final JFileChooser fc = new JFileChooser();

		senteceInput = new JTextField();
		senteceInput.setBounds(12, 43, 533, 19);
		frmSynta.getContentPane().add(senteceInput);
		senteceInput.setColumns(10);
		String sentence = "Boierii lor luau acolo prin donațiuni princiare, precum văzurăm mai sus, pămînturi, sate, munți și ape.";
		String sentence2 = "Întâmplările din Cluj trebuie să fi fost adeseori frământate cu prilejul întâlniri care aveau loc în fiecare vară.";
		String sentence3 = "Întâmplările care au avut loc vreodată pe faţa pământului nu cuprind nicio imagine pe care să n-o reproducă fantezia copioasă a norilor.";
		senteceInput.setText("Boierii lor luau acolo prin donațiuni princiare, precum văzurăm mai sus, pămînturi, sate, munți și ape.");
		//Boierii lor luau acolo prin donațiuni princiare, precum văzurăm mai sus, pămînturi, sate, munți și ape.
		//Întâmplările din Cluj trebuie să fi fost adeseori frământate cu prilejul întâlniri care aveau loc în fiecare vară.
		//IÎntâmplările care au avut loc vreodată pe faţa pământului nu cuprind nicio imagine pe care să n-o reproducă fantezia copioasă a norilor.
		JLabel lblNewLabel = new JLabel("Output tree:");
		lblNewLabel.setBounds(22, 74, 148, 15);
		frmSynta.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Insert sentece below:");
		lblNewLabel_1.setBounds(22, 16, 206, 15);
		frmSynta.getContentPane().add(lblNewLabel_1);

		JTextPane outputText = new JTextPane();
		outputText.setEditable(false);
		outputText.setBounds(12, 101, 644, 319);

		JScrollPane editorScrollPane = new JScrollPane(outputText);
		editorScrollPane.setSize(644, 324);
		editorScrollPane.setLocation(12, 96);
		editorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		editorScrollPane.setPreferredSize(new Dimension(250, 145));
		editorScrollPane.setMinimumSize(new Dimension(100, 100));
		frmSynta.getContentPane().add(editorScrollPane);

		JButton generateTreeButton = new JButton("Generate tree");
		generateTreeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (senteceInput.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please insert a sentece", "Error ", JOptionPane.ERROR_MESSAGE);
				} else {
					String output = GraphProj.getFDGParserXml("ConsoleTree", senteceInput.getText());
					outputText.setText(output);
				}
			}
		});
		generateTreeButton.setBounds(515, 432, 148, 25);
		frmSynta.getContentPane().add(generateTreeButton);

		Container contentPane = frmSynta.getContentPane();
		((JComponent) contentPane).setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		JButton generateVisualTreeButton = new JButton("Generate visual tree");
		generateVisualTreeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (senteceInput.getText().equals("")) {
					JOptionPane.showMessageDialog(null, "Please insert a sentece", "Error ", JOptionPane.ERROR_MESSAGE);
				} else {
					// SyntaxTree wordTree = GraphProj.getTree(path.getText());
					TreeForTreeLayout<TextInBox> tree = SampleTreeFactory
							.createSampleTree3(GraphProj.getFDGParserXml("VisualTree", senteceInput.getText()), senteceInput.getText());
					// TreeForTreeLayout<TextInBox> tree =
					// SampleTreeFactory.createSampleTree();
					double gapBetweenLevels = 50;
					double gapBetweenNodes = 10;
					DefaultConfiguration<TextInBox> configuration = new DefaultConfiguration<TextInBox>(
							gapBetweenLevels, gapBetweenNodes);

					// create the NodeExtentProvider for TextInBox nodes
					TextInBoxNodeExtentProvider nodeExtentProvider = new TextInBoxNodeExtentProvider();

					// create the layout
					TreeLayout<TextInBox> treeLayout = new TreeLayout<TextInBox>(tree, nodeExtentProvider,
							configuration);

					// Create a panel that draws the nodes and edges and show
					// the panel
					TextInBoxTreePane panel = new TextInBoxTreePane(treeLayout);

					JDialog dialog = new JDialog();
					Container contentPane = dialog.getContentPane();
					((JComponent) contentPane).setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
					contentPane.add(panel);
					dialog.pack();
					dialog.setLocationRelativeTo(null);
					dialog.setVisible(true);
				}
			}
		});
		generateVisualTreeButton.setBounds(322, 432, 180, 25);
		frmSynta.getContentPane().add(generateVisualTreeButton);

		JPanel panel = new JPanel();
		panel.setBounds(12, 121, 614, 281);

	}
}
