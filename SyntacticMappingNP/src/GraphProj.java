import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.axis.AxisFault;

import treeModule.*;
import treeModule.exceptions.ForbiddenTreeStructureException;
import treeModule.exceptions.NodeNotFoundException;
import uaic.webFdgRo.FdgParserRoWS_ServiceLocator;
import uaic.webNpChunkerRo.NpChunkerRoWS_ServiceLocator;
import uaic.webPosRo.PosTaggerRoWS_Service;
import uaic.webPosRo.PosTaggerRoWS_ServiceLocator;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;
import org.w3c.dom.Attr;

public class GraphProj {

	public GraphProj() {
		// TODO Auto-generated constructor stub
	}

	static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	public static Document parse(String text) throws DocumentException {
		SAXReader reader = new SAXReader();
		Document document = reader.read(text);
		return document;
	}

	public static String writeCustomXml(String type, String inputXml, String initialText) {
		String customXml = null;
		try {
			SAXReader reader = new SAXReader();
			Document document = reader.read(new StringReader(inputXml));
			Element classElement = document.getRootElement();
			
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			// root elements
			org.w3c.dom.Document doc = docBuilder.newDocument();
			org.w3c.dom.Element rootElement = doc.createElement("treebank");
			doc.appendChild(rootElement);

			Attr attr = doc.createAttribute("id");
			attr.setValue("");
			rootElement.setAttributeNode(attr);

			List<Node> nodes = document.selectNodes("/FDG_Output/S");
			for (Node node : nodes) {
				org.w3c.dom.Element staff = doc.createElement("sentence");
				rootElement.appendChild(staff);

				Attr attr2 = doc.createAttribute("id");
				attr2.setValue(node.valueOf("@id"));
				staff.setAttributeNode(attr2);

				Attr attr3 = doc.createAttribute("parser");
				attr3.setValue("");
				staff.setAttributeNode(attr3);

				Attr attr4 = doc.createAttribute("user");
				attr4.setValue("");
				staff.setAttributeNode(attr4);
				
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				Date date = new Date();

				Attr attr5 = doc.createAttribute("date");
				attr5.setValue(dateFormat.format(date));
				staff.setAttributeNode(attr5);
				
				List<Node> nodes2 = node.selectNodes("W");
				for (Node node2 : nodes2) {
					org.w3c.dom.Element element = doc.createElement("word");
					// element.appendChild(doc.createTextNode(value));
					staff.appendChild(element);
					
					String id = node2.valueOf("@id");
					String cusotmId = id.substring(2);

					Attr attr6 = doc.createAttribute("id");
					attr6.setValue(cusotmId);
					element.setAttributeNode(attr6);

					Attr attr7 = doc.createAttribute("form");
					attr7.setValue(node2.getText());
					element.setAttributeNode(attr7);

					Attr attr8 = doc.createAttribute("lemma");
					attr8.setValue(node2.valueOf("@LEMMA"));
					element.setAttributeNode(attr8);

					Attr attr9 = doc.createAttribute("postag");
					attr9.setValue(node2.valueOf("@MSD"));
					element.setAttributeNode(attr9);

					Attr attr10 = doc.createAttribute("head");
					attr10.setValue(node2.valueOf("@head"));
					element.setAttributeNode(attr10);

					Attr attr11 = doc.createAttribute("chunk");
					attr11.setValue("");
					element.setAttributeNode(attr11);

					Attr attr12 = doc.createAttribute("deprel");
					attr12.setValue(node2.valueOf("@deprel"));
					element.setAttributeNode(attr12);
				}
			}
			
			// write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", new Integer(4));
            Transformer transformer = transformerFactory.newTransformer();
    		StringWriter writer = new StringWriter();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            customXml = writer.getBuffer().toString();

		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (TransformerException tfe) {
			tfe.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		//System.out.println(customXml);
		
		if(type.equals("VisualTree")) return customXml;
		
		if(type.equals("ConsoleTree")) return generateTree(customXml, initialText);
		else return "Problem";
		

		
	}

	public static String getNPChunkerXML(String text) {
		String result = null;
		try {
			NpChunkerRoWS_ServiceLocator service = new NpChunkerRoWS_ServiceLocator();
			result = service.getNpChunkerRoWSPort().chunkText(text);
			//System.out.println(result);
		} catch (AxisFault e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public static String getFDGParserXml(String type, String text) {
		String result = null;
		try {
			FdgParserRoWS_ServiceLocator service = new FdgParserRoWS_ServiceLocator();
			result = service.getFdgParserRoWSPort().parseText(text);
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return writeCustomXml(type, result, text);
	}

	public static void callPosTagger(String text) {
		String output;
		try {
			PosTaggerRoWS_ServiceLocator serv = new PosTaggerRoWS_ServiceLocator();
			output = serv.getPosTaggerRoWSPort().parseSentence_XML(text);
			System.out.println(output);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// generateTree();

	}

	public static String generateTree(String inputText, String initialText) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			File inputFile = new File("sentenceTest.xml");
			SAXReader reader = new SAXReader();
			Document document = reader.read(inputFile);
			
//			SAXReader reader = new SAXReader();
//			Document document = reader.read(new StringReader(inputText));
			
			SyntaxSentence sentence;
			SyntaxWord word;
			SyntaxTree wordTree;
			List<SyntaxWord> wordList;
			Element classElement = document.getRootElement();

			List<Node> nodes = document.selectNodes("/treebank/sentence");
			for (Node node : nodes) {
				stringBuilder.append("----------------------------");
				stringBuilder.append("\n");
				// System.out.println("----------------------------");
				// System.out.println();
				sentence = new SyntaxSentence(node.valueOf("@id"), node.valueOf("@parser"), node.valueOf("@user"),
						node.valueOf("@date"));
				List<Node> nodes2 = node.selectNodes("word");
				wordList = new ArrayList<>();
				for (Node node2 : nodes2) {
					word = new SyntaxWord(node2.valueOf("@id"), node2.valueOf("@form"), node2.valueOf("@lemma"),
							node2.valueOf("@postag"), node2.valueOf("@head"), node2.valueOf("@chunk"),
							node2.valueOf("@deprel"));
					wordList.add(word);
				}
				wordTree = new SyntaxTree(sentence, wordList);
				wordTree.addNPChunks(getNPChunkerXML(initialText));
				// System.out.println("Tree generated from word list:\n" +
				// wordTree);
				stringBuilder.append("Tree generated from word list:\n" + wordTree);
				stringBuilder.append("NP:\n" + wordTree.getNPChunks());
				//wordTree.getAllNodesInsideNPChunks();
				// System.out.println("Tree metadata: " +
				// wordTree.getAssociatedSentenceInfo());
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (NodeNotFoundException e) {
			e.printStackTrace();
		}
		return stringBuilder.toString();
	}

	public static String readText(String path) {
		InputStreamReader r = new InputStreamReader(System.in);
		BufferedReader br2 = new BufferedReader(r);
		// path =
		// "/home/cosmin/Cosmin/Facultate/PracticaDeCercetare/syntacticmapping/text.txt";
		/*
		 * System.out.println("Enter path of txt file: "); try { path =
		 * br2.readLine(); } catch (IOException e) { e.printStackTrace(); }
		 */
		String text = null;

		try {
			text = readFile(path, StandardCharsets.UTF_8);
		} catch (IOException e) {
			e.printStackTrace();
		}
		// callPosTagger(text);
		return generateTree(path, path);
	}

	/*
	 * public static void main(String[] args) { readText(); }
	 */

	// ------------------ Hardcoded xml data --------------------
	/*
	 * SyntaxSentence sentence = new SyntaxSentence("11_citate1_977_",
	 * "Radu&apos;s parser", "Ugla", "2015-15-20"); SyntaxWord word1 = new
	 * SyntaxWord("1", "�nt�mpl�rile", "�nt�mplare", "Ncfpry", "9", "", "sbj.");
	 * SyntaxWord word2 = new SyntaxWord("2", "din", "din", "Sp", "1", "",
	 * "a.subst."); SyntaxWord word3 = new SyntaxWord("3", "Cluj", "Cluj", "Np",
	 * "2", "", "prep."); SyntaxWord word4 = new SyntaxWord("4", "trebuie",
	 * "trebui", "Vmip3s", "0", "", ""); SyntaxWord word5 = new SyntaxWord("5",
	 * "s�", "s�", "Qs", "4", "", "sbj."); SyntaxWord word6 = new
	 * SyntaxWord("6", "fi", "fi", "Van", "7", "", "aux."); SyntaxWord word7 =
	 * new SyntaxWord("7", "fost", "fi", "Vap", "9", "", "aux."); SyntaxWord
	 * word8 = new SyntaxWord("8", "adeseori", "adeseori", "Rg", "9", "",
	 * "c.c.m."); SyntaxWord word9 = new SyntaxWord("9", "fr�m�ntate",
	 * "fr�m�ntat", "Afpfprn", "5", "", "subord."); SyntaxWord word10 = new
	 * SyntaxWord("10", "cu", "cu", "Sp", "9", "", "c.c.t."); SyntaxWord word11
	 * = new SyntaxWord("11", "prilejul", "prilej", "Ncmsry", "10", "",
	 * "prep."); SyntaxWord word12 = new SyntaxWord("12", "�nt�lnirilor",
	 * "�nt�lnire", "Ncfpoy", "11", "", "a.subst."); SyntaxWord word13 = new
	 * SyntaxWord("13", "care", "care", "Pw3--r", "12", "", "a.vb."); SyntaxWord
	 * word14 = new SyntaxWord("14", "aveau loc", "avea_loc", "Vmii3p", "13",
	 * "", "subord."); SyntaxWord word15 = new SyntaxWord("15", "�n", "�n",
	 * "Sp", "14", "", "c.c.t."); SyntaxWord word16 = new SyntaxWord("16",
	 * "fiecare", "fiecare", "Pi3-sr", "17", "", "a.adj."); SyntaxWord word17 =
	 * new SyntaxWord("17", "var�", "var�", "Ncfsrn", "15", "", "prep.");
	 * SyntaxWord word18 = new SyntaxWord("18", ".", ".", "PERIOD", "4", "",
	 * "punct."); List<SyntaxWord> wordList = new ArrayList<>();
	 * wordList.add(word1); wordList.add(word2); wordList.add(word3);
	 * wordList.add(word4); wordList.add(word5); wordList.add(word6);
	 * wordList.add(word7); wordList.add(word8); wordList.add(word9);
	 * wordList.add(word10); wordList.add(word11); wordList.add(word12);
	 * wordList.add(word13); wordList.add(word14); wordList.add(word15);
	 * wordList.add(word16); wordList.add(word17); wordList.add(word18);
	 */

	// ----------------------------------------------------------

	/*
	 * NodeInterface node1 = new SyntaxNode("First"); NodeInterface node2 = new
	 * SyntaxNode("Second"); NodeInterface node3 = new SyntaxNode("Third");
	 * NodeInterface node4 = new SyntaxNode("Fourth"); NodeInterface node5 = new
	 * SyntaxNode("Fifth");
	 * 
	 * // node1.addChildNode(node2); // node1.addChildNode(node3); //
	 * node2.addChildNode(node4); // node2.addChildNode(node5);
	 * 
	 * TreeInterface tree = new SyntaxTree(); try {
	 * tree.addNode(tree.getRootNode(), node1); tree.addNode(node1, node2);
	 * tree.addNode(node1, node3); tree.addNode(node2, node4);
	 * tree.addNode(node2, node5); System.out.println(tree); List<NodeInterface>
	 * leaves = tree.getLeafNodesForNode(node2); System.out.println(
	 * "Leaf nodes for node 2:"); for (NodeInterface leaf : leaves) {
	 * System.out.print(leaf + " "); } System.out.println();
	 * 
	 * // node1.removeDescendants(); // List<NodeInterface> descendants = //
	 * tree.getRootNode().getDescendants(); // System.out.println(
	 * "\nDescendants root:"); // for(NodeInterface descendant: descendants) {
	 * // System.out.println(descendant); // } // descendants =
	 * node2.getDescendants(); // System.out.println("\nDescendants node 2:");
	 * // for(NodeInterface descendant: descendants) { //
	 * System.out.println(descendant); // }
	 * 
	 * // tree.removeNode(node2); // System.out.println("\nNew Tree: \n" +
	 * tree);
	 * 
	 * System.out.println("\nSubtree for node 2:\n" +
	 * tree.generateSubtree(node2));
	 * 
	 * } catch (NodeNotFoundException | ForbiddenTreeStructureException e) {
	 * e.printStackTrace(); }
	 */

	/*
	 * SyntaxTree wordTree; try { wordTree = new SyntaxTree(sentence, wordList);
	 * System.out.println("Tree generated from word list:\n" + wordTree);
	 * System.out.println("Tree metadata: " +
	 * wordTree.getAssociatedSentenceInfo()); } catch (NodeNotFoundException e)
	 * { e.printStackTrace(); }
	 */
}
