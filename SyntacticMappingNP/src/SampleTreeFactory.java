import java.awt.Color;
import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.abego.treelayout.TreeForTreeLayout;
import org.abego.treelayout.util.DefaultTreeForTreeLayout;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

import treeModule.NodeInterface;
import treeModule.SyntaxSentence;
import treeModule.SyntaxTree;
import treeModule.SyntaxWord;
import treeModule.exceptions.NodeNotFoundException;

public class SampleTreeFactory {
//	public static TreeForTreeLayout<TextInBox> createSampleTree() {
//		TextInBox root = new TextInBox("trebuie", 60, 20);
//		TextInBox n1 = new TextInBox("sa", 60, 20);
//		TextInBox n1_1 = new TextInBox("framantate", 80, 36);
//		TextInBox n1_1_1 = new TextInBox("Intamplarile", 60, 20);
//		TextInBox n1_1_1_1 = new TextInBox("din", 60, 20);
//		TextInBox n1_1_1_1_1 = new TextInBox("Cluj", 60, 20);
//		TextInBox n1_1_2 = new TextInBox("fost", 60, 20);
//		TextInBox n1_1_2_1 = new TextInBox("fi", 60, 20);
//		TextInBox n1_1_3 = new TextInBox("adeseori", 60, 20);
//		TextInBox n1_1_4 = new TextInBox("cu", 60, 20);
//		TextInBox n1_1_4_1 = new TextInBox("prilejul", 60, 20);
//		TextInBox n1_1_4_1_1 = new TextInBox("intalnirilor", 60, 20);
//		TextInBox n1_1_4_1_1_1 = new TextInBox("care", 60, 20);
//		TextInBox n1_1_4_1_1_1_1 = new TextInBox("aveau loc", 60, 20);
//		TextInBox n1_1_4_1_1_1_1_1 = new TextInBox("in", 60, 20);
//		TextInBox n1_1_4_1_1_1_1_1_1 = new TextInBox("vara", 60, 20);
//		TextInBox n1_1_4_1_1_1_1_1_1_1 = new TextInBox("fiecare", 60, 20);
//		TextInBox n2 = new TextInBox(".", 60, 20);
//
//
//		DefaultTreeForTreeLayout<TextInBox> tree = new DefaultTreeForTreeLayout<TextInBox>(
//				root);
//		tree.addChild(root, n1);
//		tree.addChild(n1, n1_1);
//
//		tree.addChild(n1_1, n1_1_1);
//		tree.addChild(n1_1, n1_1_2);
//		tree.addChild(n1_1, n1_1_3);
//		tree.addChild(n1_1, n1_1_4);
//
//		tree.addChild(n1_1_1, n1_1_1_1);
//		tree.addChild(n1_1_1_1, n1_1_1_1_1);
//
//		tree.addChild(n1_1_2, n1_1_2_1);
//
//		tree.addChild(n1_1_4, n1_1_4_1);
//		tree.addChild(n1_1_4_1, n1_1_4_1_1);
//		tree.addChild(n1_1_4_1_1, n1_1_4_1_1_1);
//		tree.addChild(n1_1_4_1_1_1, n1_1_4_1_1_1_1);
//		tree.addChild(n1_1_4_1_1_1_1, n1_1_4_1_1_1_1_1);
//		tree.addChild(n1_1_4_1_1_1_1_1, n1_1_4_1_1_1_1_1_1);
//		tree.addChild(n1_1_4_1_1_1_1_1_1, n1_1_4_1_1_1_1_1_1_1);
//
//		tree.addChild(root, n2);
//		return tree;
//	}

	/**
	 * @return a "Sample" tree with {@link TextInBox} items as nodes.
	 */
//	public static TreeForTreeLayout<TextInBox> createSampleTree2() {
//		TextInBox root = new TextInBox("prog", 40, 20);
//		TextInBox n1 = new TextInBox("classDef", 65, 20);
//		TextInBox n1_1 = new TextInBox("class", 50, 20);
//		TextInBox n1_2 = new TextInBox("T", 20, 20);
//		TextInBox n1_3 = new TextInBox("{", 20, 20);
//		TextInBox n1_4 = new TextInBox("member", 60, 20);
//		TextInBox n1_5 = new TextInBox("member", 60, 20);
//		TextInBox n1_5_1 = new TextInBox("<ERROR:int>", 90, 20);
//		TextInBox n1_6 = new TextInBox("member", 60, 20);
//		TextInBox n1_6_1 = new TextInBox("int", 30, 20);
//		TextInBox n1_6_2 = new TextInBox("i", 20, 20);
//		TextInBox n1_6_3 = new TextInBox(";", 20, 20);
//		TextInBox n1_7 = new TextInBox("}", 20, 20);
//
//
//		DefaultTreeForTreeLayout<TextInBox> tree = new DefaultTreeForTreeLayout<TextInBox>(
//				root);
//		tree.addChild(root, n1);
//		tree.addChild(n1, n1_1);
//		tree.addChild(n1, n1_2);
//		tree.addChild(n1, n1_3);
//		tree.addChild(n1, n1_4);
//		tree.addChild(n1, n1_5);
//		tree.addChild(n1_5, n1_5_1);
//		tree.addChild(n1, n1_6);
//		tree.addChild(n1_6,n1_6_1);
//		tree.addChild(n1_6,n1_6_2);
//		tree.addChild(n1_6,n1_6_3);
//		tree.addChild(n1, n1_7);
//		return tree;
//	}

	public static TreeForTreeLayout<TextInBox> createSampleTree3(String inputText, String initialText) {

		StringBuilder stringBuilder = new StringBuilder();
		DefaultTreeForTreeLayout<TextInBox> tree = null;
		try {
			File inputFile = new File("sentenceTest.xml");
			SAXReader reader = new SAXReader();
			Document document = reader.read(inputFile);
			
//			SAXReader reader = new SAXReader();
//			Document document = reader.read(new StringReader(inputText));
			
			SyntaxSentence sentence;
			SyntaxWord word;
			SyntaxTree wordTree = null;
			List<SyntaxWord> wordList;
			Element classElement = document.getRootElement();

			List<Node> nodes = document.selectNodes("/treebank/sentence" );
			for (Node node : nodes) {
				sentence = new SyntaxSentence(node.valueOf("@id"), node.valueOf("@parser"), node.valueOf("@user"),  node.valueOf("@date"));
				List<Node> nodes2 = node.selectNodes("word");
				wordList = new ArrayList<>();
				for (Node node2 : nodes2) {
					word = new SyntaxWord(node2.valueOf("@id"), node2.valueOf("@form"), node2.valueOf("@lemma"), node2.valueOf("@postag"), node2.valueOf("@head"), node2.valueOf("@chunk"), node2.valueOf("@deprel"));
					wordList.add(word);
				}
				wordTree = new SyntaxTree(sentence, wordList);
				wordTree.addNPChunks(GraphProj.getNPChunkerXML(initialText));
			}
			List<NodeInterface> nodes3 = wordTree.getNodes();
			HashMap<String, TextInBox> textBoxList = new HashMap<>();
			TextInBox n1 = null;
			String id = null;
			for(int i = 0; i < nodes3.size(); i++) {
				if(!wordTree.getNPChunkId(nodes3.get(i)).equals("Chunk non-existent")){
					n1 = new TextInBox(nodes3.get(i).getNodeName() + " (NP" + wordTree.getNPChunkId(nodes3.get(i)) + ")" + "\n" + "(" + nodes3.get(i).getAssociatedWord().getDeprel() + ")", 95, 20, wordTree.getNPChunkColor(nodes3.get(i)));
					textBoxList.put(nodes3.get(i).getNodeID(), n1);
				} else{
					n1 = new TextInBox(nodes3.get(i).getNodeName() + "\n" + "(" + nodes3.get(i).getAssociatedWord().getDeprel() + ")", 80, 20, wordTree.getNPChunkColor(nodes3.get(i)));
					textBoxList.put(nodes3.get(i).getNodeID(), n1);
				}
			}
			

			for(int i = 0; i < nodes3.size(); i++) {
				//System.out.println("n1: " + nodes3.get(i).getNodeID() + " " + nodes3.get(i).getNodeName());
				TextInBox node1 = textBoxList.get(nodes3.get(i).getNodeID());
				try {
					//System.out.println("n2: " + nodes3.get(i).getParentNode().getNodeID()  + " " + nodes3.get(i).getParentNode().getNodeName());
					TextInBox node2 = textBoxList.get(nodes3.get(i).getParentNode().getNodeID());
					tree.addChild(node2, node1);
				} catch(NodeNotFoundException ex) {
					//System.out.println("Is root");
					tree = new DefaultTreeForTreeLayout<TextInBox>(
							node1);
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		catch (NodeNotFoundException e) {
			e.printStackTrace();
		}
		return tree;
	}
}
