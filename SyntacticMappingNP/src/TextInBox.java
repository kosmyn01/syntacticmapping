import java.awt.Color;

public class TextInBox {

	public final String text;
	public final int height;
	public final int width;
	public final Color color;

	public TextInBox(String text, int width, int height, Color color) {
		this.text = text;
		this.width = width;
		this.height = height;
		this.color = color;
	}
}
