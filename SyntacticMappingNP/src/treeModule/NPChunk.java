package treeModule;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class NPChunk {
	final private String chunkID;
	private NodeInterface headNode;
	private List<NodeInterface> nodes;
	final private Color color;
	
	public NPChunk(String chunkID, Color color) {
		this.chunkID = chunkID;
		this.color = color;
		nodes = new ArrayList<>();
	}
	
	public void addNode(NodeInterface node) {
		nodes.add(node);
	}
	
	public void removeNode(NodeInterface node) {
		nodes.remove(node);
	}

	public String getChunkID() {
		return chunkID;
	}
	
	public Color getChunkColor() {
		return color;
	}

	public NodeInterface getHeadNode() {
		return headNode;
	}

	public void setHeadNode(NodeInterface headNode) {
		this.headNode = headNode;
	}

	public List<NodeInterface> getNodes() {
		return nodes;
	}
	
}