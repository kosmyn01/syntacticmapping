package treeModule;

import java.util.List;

import treeModule.exceptions.ForbiddenTreeStructureException;
import treeModule.exceptions.InvalidIDFormatException;
import treeModule.exceptions.NodeNotFoundException;

public interface NodeInterface extends Comparable<NodeInterface> {

	public String getNodeID();
	
	public void setNodeID(String nodeID);

	public String getNodeName();

	public void setNodeName(String nodeName);

	public NodeInterface getParentNode() throws NodeNotFoundException;

	public void setParentNode(NodeInterface parentNode);
	
	public void setAssociatedWord(SyntaxWord word);
	
	public SyntaxWord getAssociatedWord();

	public List<NodeInterface> getChildNodes();
	
	public void setChildNodes(List<NodeInterface> childNodes);

	public void addChildNode(NodeInterface childNode);

	public void removeChildNode(NodeInterface childNode)
			throws NodeNotFoundException;

	public void removeChildNode(String childNodeID)
			throws InvalidIDFormatException, NodeNotFoundException,
			ForbiddenTreeStructureException;
	
	public void removeAllChildNodes();

	public boolean hasChild(NodeInterface node);
	
	public List<NodeInterface> getDescendants();
	
	public void removeDescendants();

}
