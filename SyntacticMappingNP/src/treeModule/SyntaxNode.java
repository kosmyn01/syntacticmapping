package treeModule;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import treeModule.exceptions.InvalidIDFormatException;
import treeModule.exceptions.NodeNotFoundException;

public class SyntaxNode implements NodeInterface {

	private String nodeName;
	private String nodeID;
	private static AtomicInteger nodeCounter;
	private SyntaxNode parentNode;
	private List<NodeInterface> childNodes;
	private SyntaxWord associatedWord;
	
	// Used when creating node with same info (except ID)
	// Only used to generate root of a subtree by generateSubtree() in class SyntaxTree
	protected SyntaxNode(NodeInterface node) {
		nodeID = generateNodeID();
		setNodeName(node.getNodeName());
		if(node.getAssociatedWord() != null) {
			setAssociatedWord(node.getAssociatedWord());
		}
		childNodes = new ArrayList<>();
		List<NodeInterface> tmpChildNodes = node.getChildNodes();
		for(NodeInterface tmpChildNode: tmpChildNodes) {
			childNodes.add(new SyntaxNode(tmpChildNode, this));
		}
	}
	
	// Only used in non-root node generation of a subtree by generateSubtree() in class SyntaxTree
	protected SyntaxNode(NodeInterface newNode, NodeInterface parentNode) {
		nodeID = generateNodeID();
		setNodeName(newNode.getNodeName());
		if(newNode.getAssociatedWord() != null) {
			setAssociatedWord(newNode.getAssociatedWord());
		}
		childNodes = new ArrayList<>();
		setParentNode(parentNode);
		List<NodeInterface> tmpChildNodes = newNode.getChildNodes();
		for(NodeInterface tmpChildNode: tmpChildNodes) {
			childNodes.add(new SyntaxNode(tmpChildNode, this));
		}
	}
	
	public SyntaxNode(String nodeName) {
		if(nodeCounter == null) {
			nodeCounter = new AtomicInteger();
		}
		nodeID = generateNodeID();
		setNodeName(nodeName);
		childNodes = new ArrayList<>();
	}
	
	// MAIN CONSTRUCTOR
	public SyntaxNode(SyntaxWord word) {
		if(nodeCounter == null) {
			nodeCounter = new AtomicInteger();
		}
		nodeID = generateNodeID();
		setNodeName(word.getForm());
		childNodes = new ArrayList<>();
		setAssociatedWord(word);
	}
	
	private String generateNodeID() {
		StringBuilder id = new StringBuilder();
		int counter = nodeCounter.getAndIncrement();
		do {
			id.insert(0, convertNumberToCharacter(counter % 62));
			counter /= 62;
		} while(counter > 0);
		return id.toString();
	}
	
	private String convertNumberToCharacter(int number) {
		String output = "A";
		if(number >= 0) {
			if(number < 10) {
				output = Integer.toString(number);
			} else if(number < 36) {
				char[] character = new char[1];
				character[0] = (char)('A' - 10 + number);
				output = String.valueOf(character);
			} else if(number < 62) {
				char[] character = new char[1];
				character[0] = (char)('a' - 36 + number);
				output = String.valueOf(character);
			} else {
				System.out.println("Number too high brah");
			}
		}
		return output;
	}
	
	@Override
	public String getNodeID() {
		return nodeID;
	}
	
	public void setNodeID(String nodeID) {
		this.nodeID = nodeID;
	}

	@Override
	public String getNodeName() {
		return nodeName;
	}

	@Override
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	@Override
	public NodeInterface getParentNode() throws NodeNotFoundException {
		if(parentNode == null) {
			throw new NodeNotFoundException("In SyntaxNode(NodeID: " + getNodeID()
					+ "): This node has no parent node");
		}
		return parentNode;
	}

	@Override
	public void setParentNode(NodeInterface parentNode) {
		this.parentNode = (SyntaxNode)parentNode;
	}

	@Override
	public List<NodeInterface> getChildNodes() {
		return childNodes;
	}
	
	@Override
	public void setChildNodes(List<NodeInterface> childNodes) {
		if(childNodes != null) {
			this.childNodes = childNodes;
		}
	}

	@Override
	public void addChildNode(NodeInterface childNode) {
		if(!childNodes.contains(childNode)) {
			childNode.setParentNode(this);
			childNodes.add(childNode);
		}
	}

	@Override
	public void removeChildNode(NodeInterface childNode)
			throws NodeNotFoundException {
		if(childNodes.contains(childNode)) {
			childNode.setParentNode(null);
			childNodes.remove(childNode);
		} else {
			throw new NodeNotFoundException("In SyntaxNode(NodeID: " + getNodeID()
					+ "): The node to be removed" + " is not a child of this node");
		}
	}

	@Override
	public void removeChildNode(String childNodeID)
			throws InvalidIDFormatException, NodeNotFoundException {
		if(!isIDFormatValid(childNodeID)) {
			throw new InvalidIDFormatException("In SyntaxNode(NodeID: " + getNodeID()
					+ "): The input format is not valid");
		}
		boolean nodeFound = false;
		for(NodeInterface node: childNodes) {
			if(node.getNodeID().equals(childNodeID)) {
				node.setParentNode(null);
				childNodes.remove(node);
				nodeFound = true;
				break;
			}
		}
		if(!nodeFound) {
			throw new NodeNotFoundException("In SyntaxNode(NodeID: " + getNodeID()
					+ "): The node to be removed" + " is not a child of this node");
		}
	}
	
	@Override
	public void removeAllChildNodes() {
		// using temporary list because a list cannot be modified while iterating through it
		List<NodeInterface> tmpChildNodeList = new ArrayList<>(childNodes);
		for(NodeInterface node: tmpChildNodeList) {
			childNodes.remove(node);
			node.setParentNode(null);
		}
	}
	
	private boolean isIDFormatValid(String id) {
		return id.matches("^[a-zA-Z0-9]*$");
	}

	@Override
	public boolean hasChild(NodeInterface node) {
		return childNodes.contains(node);
	}

	@Override
	public List<NodeInterface> getDescendants() {
		List<NodeInterface> output = new ArrayList<>(childNodes);
		for(NodeInterface node: childNodes) {
			output.addAll(node.getDescendants());
		}
		return output;
	}

	@Override
	public void removeDescendants() {
		List<NodeInterface> descendants = getDescendants();
		for(NodeInterface descendant: descendants) {
			descendant.removeAllChildNodes();
			descendant.setParentNode(null);
		}
		removeAllChildNodes();
	}

	@Override
	public SyntaxWord getAssociatedWord() {
		return associatedWord;
	}

	@Override
	public void setAssociatedWord(SyntaxWord associatedWord) {
		this.associatedWord = associatedWord;
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder(nodeName + "(ID=" + nodeID);
		if(associatedWord != null) {
			output.append(", " + "xml_ID=" + associatedWord.getId());
		}
		output.append(")");
		return output.toString();
	}

	@Override
	public int compareTo(NodeInterface node) {
		return nodeID.compareTo(node.getNodeID());
	}
	
}
