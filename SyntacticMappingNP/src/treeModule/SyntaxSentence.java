package treeModule;

public class SyntaxSentence {

	private String id;
	private String parser;
	private String user;
	private String date;

	public SyntaxSentence() {
		
	}
	
	public SyntaxSentence(String id, String parser, String user, String date) {
		this.id = id;
		this.parser = parser;
		this.user = user;
		this.date = date;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParser() {
		return parser;
	}

	public void setParser(String parser) {
		this.parser = parser;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		output.append("Sentence id = ").append(id);
		output.append(" | parser = ").append(parser);
		output.append(" | user = ").append(user);
		output.append(" | date = ").append(date);
		return output.toString();
	}

}
