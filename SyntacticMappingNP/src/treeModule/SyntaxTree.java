package treeModule;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import treeModule.exceptions.ForbiddenTreeStructureException;
import treeModule.exceptions.InvalidIDFormatException;
import treeModule.exceptions.NodeNotFoundException;
import xmlParsers.NPChunkerParser;

public class SyntaxTree implements TreeInterface {

	private NodeInterface rootNode;
	private List<NodeInterface> nodes;
	private List<NPChunk> npChunks = new ArrayList<>();
	private int chunkCounter = 0;
	
	/*
	 * A tree is associated with a sentence, therefore it has a
	 * associatedSentence object that stores all information about the
	 * respective sentence read from the xml provided by the NP-Chunker service
	 */
	private SyntaxSentence associatedSentenceInfo;

	public SyntaxTree() {
		rootNode = new SyntaxNode("S");
		setNodes(new ArrayList<>());
		getNodes().add(rootNode);
		associatedSentenceInfo = new SyntaxSentence("none", "none", "none", "none");
	}

	public SyntaxTree(NodeInterface rootNode) {
		this.rootNode = rootNode;
		setNodes(new ArrayList<>());
		getNodes().add(this.rootNode);
		getNodes().addAll(rootNode.getDescendants());
		associatedSentenceInfo = new SyntaxSentence("none", "none", "none", "none");
	}

	// MAIN CONSTRUCTOR
	// Trees generated from word lists extracted from xml will always have nodes
	// stored in BFS order
	public SyntaxTree(SyntaxSentence sentence, List<SyntaxWord> wordList) throws NodeNotFoundException {
		setNodes(new ArrayList<>());
		associatedSentenceInfo = sentence;
		// Searching for root node. Throw exception if a root node was not found
		for (SyntaxWord word : wordList) {
			if (word.getHead().equals("0")) {
				rootNode = new SyntaxNode(word);
				break;
			}
		}
		if (rootNode == null) {
			throw new NodeNotFoundException("In SyntaxTree: Root node has not been found");
		}
		getNodes().add(rootNode);
		wordList.remove(rootNode.getAssociatedWord());

		// Constructing
		List<NodeInterface> nodesToBeProcessed = new ArrayList<>();
		nodesToBeProcessed.add(rootNode);
		while (!nodesToBeProcessed.isEmpty()) {
			NodeInterface currentNode = nodesToBeProcessed.get(0);
			for (SyntaxWord word : wordList) {
				if (currentNode.getAssociatedWord().getId().equals(word.getHead())) {
					try {
						SyntaxNode newNode = new SyntaxNode(word);
						addNode(currentNode, newNode);
						nodesToBeProcessed.add(newNode);
					} catch (ForbiddenTreeStructureException e) {
						e.printStackTrace();
					}
				}
			}
			nodesToBeProcessed.remove(currentNode);
		}
	}

	@Override
	public NodeInterface getRootNode() {
		return rootNode;
	}

	@Override
	public void addNode(NodeInterface parentNode, NodeInterface childNode)
			throws NodeNotFoundException, ForbiddenTreeStructureException {
		boolean parentNodeFound = false;
		boolean childNodeFound = false;
		for (NodeInterface node : getNodes()) {
			if (node.getNodeID().equals(parentNode.getNodeID())) {
				parentNodeFound = true;
			}
			if (node.getNodeID().equals(childNode.getNodeID())) {
				childNodeFound = true;
			}
		}
		if (!parentNodeFound) {
			throw new NodeNotFoundException("In SyntaxTree: Parent node not found in tree");
		}
		if (childNodeFound) {
			throw new ForbiddenTreeStructureException("In SyntaxTree: Child node must not exist in tree");
		}
		parentNode.addChildNode(childNode);
		getNodes().add(childNode);
	}

	@Override
	public void removeNode(NodeInterface node) throws NodeNotFoundException, ForbiddenTreeStructureException {
		if (!getNodes().contains(node)) {
			throw new NodeNotFoundException("In SyntaxTree: Node to be removed was not found");
		}
		if (node.getNodeID() == rootNode.getNodeID()) {
			throw new ForbiddenTreeStructureException("In SyntaxTree: Tree cannot exist without root node");
		}
		getNodes().removeAll(node.getDescendants());
		getNodes().remove(node);
		node.setParentNode(null);
		node.removeDescendants();
	}

	@Override
	public NodeInterface getNodeWithID(String nodeID) throws NodeNotFoundException, InvalidIDFormatException {
		if (!isIDFormatValid(nodeID)) {
			throw new InvalidIDFormatException("In SyntaxTree: Input node ID not valid");
		}
		for (NodeInterface node : getNodes()) {
			if (node.getNodeID().equals(nodeID)) {
				return node;
			}
		}
		throw new NodeNotFoundException("In SyntaxTree: Tree does not contain any node with ID: " + nodeID);
	}

	@Override
	public List<NodeInterface> getLeafNodes() {
		List<NodeInterface> output = new ArrayList<>();
		for (NodeInterface node : getNodes()) {
			if (node.getChildNodes().isEmpty()) {
				output.add(node);
			}
		}
		return output;
	}

	@Override
	public List<NodeInterface> getLeafNodesForNode(NodeInterface node) throws NodeNotFoundException {
		if (!getNodes().contains(node)) {
			throw new NodeNotFoundException("In SyntaxTree: Node was not found in tree");
		}
		List<NodeInterface> output = new ArrayList<>();
		List<NodeInterface> descendants = node.getDescendants();
		for (NodeInterface descendant : descendants) {
			if (descendant.getChildNodes().isEmpty()) {
				output.add(descendant);
			}
		}
		return output;
	}

	@Override
	public List<NodeInterface> getNodesWithName(String name) {
		List<NodeInterface> output = new ArrayList<>();
		for (NodeInterface node : getNodes()) {
			if (node.getNodeName().equals(name)) {
				output.add(node);
			}
		}
		return output;
	}

	@Override
	public List<NodeInterface> getHeadNodes() {
		return getHeadNodes(1);
	}

	@Override
	public List<NodeInterface> getHeadNodes(int level) {
		List<NodeInterface> output = new ArrayList<>();
		List<NodeInterface> leaves = getLeafNodes();
		for (NodeInterface leaf : leaves) {
			try {
				NodeInterface headNode = leaf.getParentNode();
				for (int i = 1; i < level; i++) {
					try {
						headNode = headNode.getParentNode();
					} catch (NodeNotFoundException e) {

					}
				}
				if (!output.contains(headNode)) {
					output.add(headNode);
				}
			} catch (NodeNotFoundException e) {

			}
		}
		return output;
	}

	@Override
	public TreeInterface generateSubtree(NodeInterface rootNode) throws NodeNotFoundException {
		return new SyntaxTree(new SyntaxNode(rootNode));
	}

	public boolean isIDFormatValid(String id) {
		return id.matches("[a-zA-Z0-9]+(_[a-zA-Z0-9]+)*");
	}

	@Override
	public SyntaxSentence getAssociatedSentenceInfo() {
		return associatedSentenceInfo;
	}

	@Override
	public String toString() {
		StringBuilder output = new StringBuilder();
		for (NodeInterface node : getNodes()) {
			output.append("Node: " + node + " | Parent: ");
			// System.out.println("n11: " + node.getNodeID() + " " +
			// node.getNodeName());
			try {
				output.append(node.getParentNode());
				// System.out.println("n22: " + node.getParentNode().getNodeID()
				// + " " + node.getParentNode().getNodeName());
			} catch (NodeNotFoundException ex) {
				output.append("None");
			}
			output.append(" | Children: ");
			List<NodeInterface> childNodes = node.getChildNodes();
			if (childNodes.isEmpty()) {
				output.append("None");
			} else {
				for (NodeInterface childNode : childNodes) {
					output.append(childNode + " ");
					// System.out.println("n33: " + childNode.getNodeID() + " "
					// + childNode.getNodeName());
				}
			}
			output.append("\n");
		}
		return output.toString();
	}

	public List<NodeInterface> getNodes() {
		return nodes;
	}

	public void setNodes(List<NodeInterface> nodes) {
		this.nodes = nodes;
	}

	@Override
	public void mergeNodes(String nodeID1, String nodeID2)
			throws NodeNotFoundException, InvalidIDFormatException, ForbiddenTreeStructureException {
		NodeInterface node1 = getNodeWithID(nodeID1);
		NodeInterface node2 = getNodeWithID(nodeID2);
		if (node1.getChildNodes().contains(node2)) {
			// The first node is the parent of the second node
			mergeNodes(node1, node2);
		} else if (node2.getChildNodes().contains(node1)) {
			// The second node is the parent of the first node
			mergeNodes(node2, node1);
		} else {
			// Nodes are not directly connected
			throw new ForbiddenTreeStructureException(
					"Nodes " + nodeID1 + " and " + nodeID2 + " are not directly connected");
		}
	}
	
	private void mergeNodes(NodeInterface node1, NodeInterface node2) {
		if(!node1.equals(node2)) {
			node1.setAssociatedWord(mergeSyntaxWords(node1.getAssociatedWord(), node2.getAssociatedWord()));
			node1.setNodeID(node1.getNodeID() + "_" + node2.getNodeID());
			node1.setNodeName(node1.getNodeName() + " " + node2.getNodeName());
			List<NodeInterface> newChildNodeList = node1.getChildNodes();
			newChildNodeList.addAll(node2.getChildNodes());
			node1.setChildNodes(newChildNodeList);
			nodes.remove(node2);
		}
	}
	
	private SyntaxWord mergeSyntaxWords(SyntaxWord word1, SyntaxWord word2) {
		SyntaxWord output = new SyntaxWord();
		if(word1 != null && word2 != null) {
			output.setId(word1.getId() + "_" + word2.getId());
			output.setForm(word1.getForm() + "_" + word2.getForm());
			output.setLemma(word1.getLemma() + "_" + word2.getLemma());
			output.setPostag(word1.getPostag() + "_" + word2.getPostag());
			output.setHead(word1.getHead() + "_" + word2.getHead());
			output.setChunk(word1.getChunk() + "_" + word2.getChunk());
			output.setDeprel(word1.getDeprel() + "_" + word2.getDeprel());
		}
		return output;
	}
	
	@SuppressWarnings("unused")
	private NodeInterface getNodeWithPseudoID(String pseudoID) throws NodeNotFoundException {
		NodeInterface output = null;
		if(rootNode.getAssociatedWord().getId().equals(pseudoID)) {
			return rootNode;
		} else {
			for(NodeInterface node: nodes) {
				if(node.getAssociatedWord().getId().equals(pseudoID)) {
					return node;
				}
			}
		}
		if(output == null) {
			throw new NodeNotFoundException("The tree does not have any nodes with " 
					+ "the pseudo-ID: " + pseudoID);
		}
		return output;
	}

	@Override
	public void addNPChunks(String chunkerXML) {
		List<List<String>> npChunksInfo = NPChunkerParser.getNPChunks(chunkerXML);
		addNPChunks(npChunksInfo);
	}
	
	private void addNPChunks(List<List<String>> npChunksInfo) {
		for(List<String> singleChunkInfo: npChunksInfo) {
			addNPChunk(singleChunkInfo);
		}
	}
	
	private void addNPChunk(List<String> nodeIDs) {
		try {
			Random random = new Random();
			final float hue = random.nextFloat();
			final float saturation = 0.9f;//1.0 for brilliant, 0.0 for dull
			final float luminance = 1.0f; //1.0 for brighter, 0.0 for black
			Color color = Color.getHSBColor(hue, saturation, luminance);
			NPChunk chunk = new NPChunk(Integer.toString(chunkCounter++), Color.yellow);
			chunk.setHeadNode(getNodeWithPseudoID(nodeIDs.get(0)));
			for(int i = 1; i < nodeIDs.size(); i++) {
				chunk.addNode(getNodeWithPseudoID(nodeIDs.get(i)));
			}
			npChunks.add(chunk);
		} catch (NodeNotFoundException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getNPChunks(){
		StringBuilder output = new StringBuilder();
		for(NPChunk chunk: npChunks) {
			output.append("Chunk " + chunk.getChunkID() + ": ");
			// sorting by IDs to get words in the order appeared in the sentence
			List<NodeInterface> nodeList = new ArrayList<>(chunk.getNodes());
			nodeList.add(chunk.getHeadNode());
			Collections.sort(nodeList);
			for(NodeInterface node: nodeList) {
				output.append(node.toString() + " ");
			}
			output.append("\n");
		}
		return output.toString();
	}

	@Override
	public List<NodeInterface> getAllNodesInsideNPChunks() {
		List<NodeInterface> output = new ArrayList<>();
		for(NPChunk chunk: npChunks) {
			output.add(chunk.getHeadNode());
			output.addAll(chunk.getNodes());
		}
		return output;
	}

	@Override
	public String getNPChunkId(NodeInterface node) {
		for(NPChunk chunk: npChunks) {
			List<NodeInterface> chunkComponents = chunk.getNodes();
			chunkComponents.add(chunk.getHeadNode());
			for(NodeInterface chunkComponent: chunkComponents) {
				if(chunkComponent.getNodeID().equals(node.getNodeID())) {
					return chunk.getChunkID();
				}
			}
		}
		return "Chunk non-existent";
	}
	
	@Override
	public Color getNPChunkColor(NodeInterface node) {
		for(NPChunk chunk: npChunks) {
			List<NodeInterface> chunkComponents = chunk.getNodes();
			chunkComponents.add(chunk.getHeadNode());
			for(NodeInterface chunkComponent: chunkComponents) {
				if(chunkComponent.getNodeID().equals(node.getNodeID())) {
					return chunk.getChunkColor();
				}
			}
		}
		return Color.white;
	}

}
