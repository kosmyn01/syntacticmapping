package treeModule;

/*
 * A SyntaxWord object is associated with a SyntaxNode object
 */
public class SyntaxWord {
	
	private String id;
	private String form;
	private String lemma;
	private String postag;
	private String head;
	private String chunk;
	private String deprel;

	public SyntaxWord() {
		
	}
	
	public SyntaxWord(String id, String form, String lemma,
			String postag, String head, String chunk, String deprel) {
		this.id = id;
		this.form = form;
		this.lemma = lemma;
		this.postag = postag;
		this.head = head;
		this.chunk = chunk;
		this.deprel = deprel;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	public String getLemma() {
		return lemma;
	}

	public void setLemma(String lemma) {
		this.lemma = lemma;
	}

	public String getPostag() {
		return postag;
	}

	public void setPostag(String postag) {
		this.postag = postag;
	}

	public String getHead() {
		return head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getChunk() {
		return chunk;
	}

	public void setChunk(String chunk) {
		this.chunk = chunk;
	}

	public String getDeprel() {
		return deprel;
	}

	public void setDeprel(String deprel) {
		this.deprel = deprel;
	}
}
