package treeModule;

import java.awt.Color;
import java.util.List;

import treeModule.exceptions.ForbiddenTreeStructureException;
import treeModule.exceptions.InvalidIDFormatException;
import treeModule.exceptions.NodeNotFoundException;

public interface TreeInterface {

	public NodeInterface getRootNode();

	public void addNode(NodeInterface parentNode, NodeInterface childNode)
			throws NodeNotFoundException, ForbiddenTreeStructureException;

	/*
	 * removeNode method should also remove any nodes that are children, etc, of
	 * respective node
	 */
	public void removeNode(NodeInterface node) throws NodeNotFoundException, ForbiddenTreeStructureException;

	/*
	 * Only two nodes that are directly connected can be merged;
	 * otherwise, an ForbiddenTreeStructureException is thrown
	 */
	public void mergeNodes(String nodeID1, String nodeID2)
			throws NodeNotFoundException, InvalidIDFormatException, ForbiddenTreeStructureException;

	public NodeInterface getNodeWithID(String nodeID) throws NodeNotFoundException, InvalidIDFormatException;

	public List<NodeInterface> getLeafNodes();

	public List<NodeInterface> getLeafNodesForNode(NodeInterface node) throws NodeNotFoundException;

	/*
	 * The method below is useful if for retrieving non-leaf nodes For our
	 * example(syntactic mapping - NP)
	 */
	public List<NodeInterface> getNodesWithName(String name) throws NodeNotFoundException;

	/*
	 * A level of 1 returns a list with the head nodes of the leaf nodes A level
	 * of 2 returns a list with the head nodes of the nodes returned by
	 * getHeadNodes(1) ... etc
	 */
	public List<NodeInterface> getHeadNodes(int level);

	public List<NodeInterface> getHeadNodes();

	/*
	 * The method below returns the subtree of this tree that has 'rootNode' as
	 * the root
	 */
	public TreeInterface generateSubtree(NodeInterface rootNode) throws NodeNotFoundException;

	/*
	 * Retrieves metadata about the tree (specifically, about the sentence it
	 * represents)
	 */
	public SyntaxSentence getAssociatedSentenceInfo();
	
	/*
	 * Reads a list of NP chunks from the xml provided by the Chunker Service
	 */
	public void addNPChunks(String chunkerXMLPath);
	
	/*
	 * Returns a string with all the NP chunks + every word of that chunk
	 */
	public String getNPChunks();
	
	public List<NodeInterface> getAllNodesInsideNPChunks();
	
	public String getNPChunkId(NodeInterface node);

	Color getNPChunkColor(NodeInterface node);

}
