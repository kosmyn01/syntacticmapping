package treeModule.exceptions;

public class ForbiddenTreeStructureException extends Exception {

	private static final long serialVersionUID = -2469514369685625443L;

	public ForbiddenTreeStructureException() {
		
	}
	
	public ForbiddenTreeStructureException(String message) {
		super(message);
	}
}
