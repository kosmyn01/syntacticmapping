package treeModule.exceptions;

public class InvalidIDFormatException extends Exception {
	
	private static final long serialVersionUID = 76062405993928401L;

	public InvalidIDFormatException() {
		
	}
	
	public InvalidIDFormatException(String message) {
		super(message);
	}
}
