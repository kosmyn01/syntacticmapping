package treeModule.exceptions;

public class NodeNotFoundException extends Exception {

	private static final long serialVersionUID = 5059885947740054885L;
	
	public NodeNotFoundException() {
		
	}
	
	public NodeNotFoundException(String message) {
		super(message);
	}
}
