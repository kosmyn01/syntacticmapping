package uaic.webFdgRo;

public class FdgParserRoWSProxy implements uaic.webFdgRo.FdgParserRoWS_PortType {
  private String _endpoint = null;
  private uaic.webFdgRo.FdgParserRoWS_PortType fdgParserRoWS_PortType = null;
  
  public FdgParserRoWSProxy() {
    _initFdgParserRoWSProxy();
  }
  
  public FdgParserRoWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initFdgParserRoWSProxy();
  }
  
  private void _initFdgParserRoWSProxy() {
    try {
      fdgParserRoWS_PortType = (new uaic.webFdgRo.FdgParserRoWS_ServiceLocator()).getFdgParserRoWSPort();
      if (fdgParserRoWS_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)fdgParserRoWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)fdgParserRoWS_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (fdgParserRoWS_PortType != null)
      ((javax.xml.rpc.Stub)fdgParserRoWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public uaic.webFdgRo.FdgParserRoWS_PortType getFdgParserRoWS_PortType() {
    if (fdgParserRoWS_PortType == null)
      _initFdgParserRoWSProxy();
    return fdgParserRoWS_PortType;
  }
  
  public java.lang.String parsePosTaggedXML(java.lang.String posTaggedXML) throws java.rmi.RemoteException{
    if (fdgParserRoWS_PortType == null)
      _initFdgParserRoWSProxy();
    return fdgParserRoWS_PortType.parsePosTaggedXML(posTaggedXML);
  }
  
  public java.lang.String parseText(java.lang.String txt) throws java.rmi.RemoteException{
    if (fdgParserRoWS_PortType == null)
      _initFdgParserRoWSProxy();
    return fdgParserRoWS_PortType.parseText(txt);
  }
  
  
}