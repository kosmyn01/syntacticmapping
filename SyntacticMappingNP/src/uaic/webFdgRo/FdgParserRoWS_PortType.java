/**
 * FdgParserRoWS_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uaic.webFdgRo;

public interface FdgParserRoWS_PortType extends java.rmi.Remote {
    public java.lang.String parsePosTaggedXML(java.lang.String posTaggedXML) throws java.rmi.RemoteException;
    public java.lang.String parseText(java.lang.String txt) throws java.rmi.RemoteException;
}
