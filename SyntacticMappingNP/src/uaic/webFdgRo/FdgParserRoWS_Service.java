/**
 * FdgParserRoWS_Service.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uaic.webFdgRo;

public interface FdgParserRoWS_Service extends javax.xml.rpc.Service {
    public java.lang.String getFdgParserRoWSPortAddress();

    public uaic.webFdgRo.FdgParserRoWS_PortType getFdgParserRoWSPort() throws javax.xml.rpc.ServiceException;

    public uaic.webFdgRo.FdgParserRoWS_PortType getFdgParserRoWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
