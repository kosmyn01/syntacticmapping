/**
 * FdgParserRoWS_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uaic.webFdgRo;

public class FdgParserRoWS_ServiceLocator extends org.apache.axis.client.Service implements uaic.webFdgRo.FdgParserRoWS_Service {

    public FdgParserRoWS_ServiceLocator() {
    }


    public FdgParserRoWS_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public FdgParserRoWS_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for FdgParserRoWSPort
    private java.lang.String FdgParserRoWSPort_address = "http://nlptools.info.uaic.ro:80/WebFdgRo/FdgParserRoWS";

    public java.lang.String getFdgParserRoWSPortAddress() {
        return FdgParserRoWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String FdgParserRoWSPortWSDDServiceName = "FdgParserRoWSPort";

    public java.lang.String getFdgParserRoWSPortWSDDServiceName() {
        return FdgParserRoWSPortWSDDServiceName;
    }

    public void setFdgParserRoWSPortWSDDServiceName(java.lang.String name) {
        FdgParserRoWSPortWSDDServiceName = name;
    }

    public uaic.webFdgRo.FdgParserRoWS_PortType getFdgParserRoWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(FdgParserRoWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getFdgParserRoWSPort(endpoint);
    }

    public uaic.webFdgRo.FdgParserRoWS_PortType getFdgParserRoWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            uaic.webFdgRo.FdgParserRoWSPortBindingStub _stub = new uaic.webFdgRo.FdgParserRoWSPortBindingStub(portAddress, this);
            _stub.setPortName(getFdgParserRoWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setFdgParserRoWSPortEndpointAddress(java.lang.String address) {
        FdgParserRoWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (uaic.webFdgRo.FdgParserRoWS_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                uaic.webFdgRo.FdgParserRoWSPortBindingStub _stub = new uaic.webFdgRo.FdgParserRoWSPortBindingStub(new java.net.URL(FdgParserRoWSPort_address), this);
                _stub.setPortName(getFdgParserRoWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("FdgParserRoWSPort".equals(inputPortName)) {
            return getFdgParserRoWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webFdgRo.uaic/", "FdgParserRoWS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webFdgRo.uaic/", "FdgParserRoWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("FdgParserRoWSPort".equals(portName)) {
            setFdgParserRoWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
