package uaic.webNpChunkerRo;

public class NpChunkerRoWSProxy implements uaic.webNpChunkerRo.NpChunkerRoWS_PortType {
  private String _endpoint = null;
  private uaic.webNpChunkerRo.NpChunkerRoWS_PortType npChunkerRoWS_PortType = null;
  
  public NpChunkerRoWSProxy() {
    _initNpChunkerRoWSProxy();
  }
  
  public NpChunkerRoWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initNpChunkerRoWSProxy();
  }
  
  private void _initNpChunkerRoWSProxy() {
    try {
      npChunkerRoWS_PortType = (new uaic.webNpChunkerRo.NpChunkerRoWS_ServiceLocator()).getNpChunkerRoWSPort();
      if (npChunkerRoWS_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)npChunkerRoWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)npChunkerRoWS_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (npChunkerRoWS_PortType != null)
      ((javax.xml.rpc.Stub)npChunkerRoWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public uaic.webNpChunkerRo.NpChunkerRoWS_PortType getNpChunkerRoWS_PortType() {
    if (npChunkerRoWS_PortType == null)
      _initNpChunkerRoWSProxy();
    return npChunkerRoWS_PortType;
  }
  
  public java.lang.String chunkTaggedText(java.lang.String taggedXml) throws java.rmi.RemoteException{
    if (npChunkerRoWS_PortType == null)
      _initNpChunkerRoWSProxy();
    return npChunkerRoWS_PortType.chunkTaggedText(taggedXml);
  }
  
  public java.lang.String chunkText(java.lang.String inputText) throws java.rmi.RemoteException{
    if (npChunkerRoWS_PortType == null)
      _initNpChunkerRoWSProxy();
    return npChunkerRoWS_PortType.chunkText(inputText);
  }
  
  
}