/**
 * NpChunkerRoWS_PortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uaic.webNpChunkerRo;

public interface NpChunkerRoWS_PortType extends java.rmi.Remote {
    public java.lang.String chunkTaggedText(java.lang.String taggedXml) throws java.rmi.RemoteException;
    public java.lang.String chunkText(java.lang.String inputText) throws java.rmi.RemoteException;
}
