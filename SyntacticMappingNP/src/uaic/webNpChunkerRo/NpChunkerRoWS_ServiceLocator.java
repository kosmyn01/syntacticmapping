/**
 * NpChunkerRoWS_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package uaic.webNpChunkerRo;

public class NpChunkerRoWS_ServiceLocator extends org.apache.axis.client.Service implements uaic.webNpChunkerRo.NpChunkerRoWS_Service {

    public NpChunkerRoWS_ServiceLocator() {
    }


    public NpChunkerRoWS_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public NpChunkerRoWS_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for NpChunkerRoWSPort
    private java.lang.String NpChunkerRoWSPort_address = "http://nlptools.info.uaic.ro:80/WebNpChunkerRo/NpChunkerRoWS";

    public java.lang.String getNpChunkerRoWSPortAddress() {
        return NpChunkerRoWSPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String NpChunkerRoWSPortWSDDServiceName = "NpChunkerRoWSPort";

    public java.lang.String getNpChunkerRoWSPortWSDDServiceName() {
        return NpChunkerRoWSPortWSDDServiceName;
    }

    public void setNpChunkerRoWSPortWSDDServiceName(java.lang.String name) {
        NpChunkerRoWSPortWSDDServiceName = name;
    }

    public uaic.webNpChunkerRo.NpChunkerRoWS_PortType getNpChunkerRoWSPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(NpChunkerRoWSPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getNpChunkerRoWSPort(endpoint);
    }

    public uaic.webNpChunkerRo.NpChunkerRoWS_PortType getNpChunkerRoWSPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            uaic.webNpChunkerRo.NpChunkerRoWSPortBindingStub _stub = new uaic.webNpChunkerRo.NpChunkerRoWSPortBindingStub(portAddress, this);
            _stub.setPortName(getNpChunkerRoWSPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setNpChunkerRoWSPortEndpointAddress(java.lang.String address) {
        NpChunkerRoWSPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (uaic.webNpChunkerRo.NpChunkerRoWS_PortType.class.isAssignableFrom(serviceEndpointInterface)) {
                uaic.webNpChunkerRo.NpChunkerRoWSPortBindingStub _stub = new uaic.webNpChunkerRo.NpChunkerRoWSPortBindingStub(new java.net.URL(NpChunkerRoWSPort_address), this);
                _stub.setPortName(getNpChunkerRoWSPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("NpChunkerRoWSPort".equals(inputPortName)) {
            return getNpChunkerRoWSPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://webNpChunkerRo.uaic/", "NpChunkerRoWS");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://webNpChunkerRo.uaic/", "NpChunkerRoWSPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("NpChunkerRoWSPort".equals(portName)) {
            setNpChunkerRoWSPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
