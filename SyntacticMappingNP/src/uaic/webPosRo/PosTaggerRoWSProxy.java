package uaic.webPosRo;

public class PosTaggerRoWSProxy implements uaic.webPosRo.PosTaggerRoWS_PortType {
  private String _endpoint = null;
  private uaic.webPosRo.PosTaggerRoWS_PortType posTaggerRoWS_PortType = null;
  
  public PosTaggerRoWSProxy() {
    _initPosTaggerRoWSProxy();
  }
  
  public PosTaggerRoWSProxy(String endpoint) {
    _endpoint = endpoint;
    _initPosTaggerRoWSProxy();
  }
  
  private void _initPosTaggerRoWSProxy() {
    try {
      posTaggerRoWS_PortType = (new uaic.webPosRo.PosTaggerRoWS_ServiceLocator()).getPosTaggerRoWSPort();
      if (posTaggerRoWS_PortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)posTaggerRoWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)posTaggerRoWS_PortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (posTaggerRoWS_PortType != null)
      ((javax.xml.rpc.Stub)posTaggerRoWS_PortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public uaic.webPosRo.PosTaggerRoWS_PortType getPosTaggerRoWS_PortType() {
    if (posTaggerRoWS_PortType == null)
      _initPosTaggerRoWSProxy();
    return posTaggerRoWS_PortType;
  }
  
  public java.lang.String parseSentence_TXT(java.lang.String rawSentenceInput) throws java.rmi.RemoteException{
    if (posTaggerRoWS_PortType == null)
      _initPosTaggerRoWSProxy();
    return posTaggerRoWS_PortType.parseSentence_TXT(rawSentenceInput);
  }
  
  public java.lang.String parseSentence_XML(java.lang.String rawSentenceInput) throws java.rmi.RemoteException{
    if (posTaggerRoWS_PortType == null)
      _initPosTaggerRoWSProxy();
    return posTaggerRoWS_PortType.parseSentence_XML(rawSentenceInput);
  }
  
  public java.lang.String parseText_XML(java.lang.String rawTextInput) throws java.rmi.RemoteException{
    if (posTaggerRoWS_PortType == null)
      _initPosTaggerRoWSProxy();
    return posTaggerRoWS_PortType.parseText_XML(rawTextInput);
  }
  
  public java.lang.String parseText_TXT(java.lang.String rawTextInput) throws java.rmi.RemoteException{
    if (posTaggerRoWS_PortType == null)
      _initPosTaggerRoWSProxy();
    return posTaggerRoWS_PortType.parseText_TXT(rawTextInput);
  }
  
  
}