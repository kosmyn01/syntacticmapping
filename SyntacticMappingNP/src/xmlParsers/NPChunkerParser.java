package xmlParsers;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.dom4j.io.SAXReader;

/*
 * Works for only 1 sentence at the moment :(
 */
public class NPChunkerParser {
	@SuppressWarnings("unchecked")
	public static List<List<String>> getNPChunks(String inputXML) {
		List<List<String>> output = new ArrayList<>();
		
        try {
        	//File inputFile = new File(path);
//            SAXReader reader = new SAXReader();
//			Document document = reader.read(new StringReader(inputXML));
        	
        	File inputFile = new File("output_NpChunkerRo.xml");
			SAXReader reader = new SAXReader();
			Document document = reader.read(inputFile);
        	
			Node sentence = document.selectSingleNode("POS_Output").selectSingleNode("S");
			List<Node> nodes = sentence.selectNodes("NP" );
			for(Node node: nodes) {
				Node headWord = node.selectSingleNode("HEAD").selectSingleNode("W");
//				System.out.println(headWord.getText() + " id = "
//						+ headWord.valueOf("@id").split("\\.")[1]);
				if(headWord != null) {
					List<String> npChunk = new ArrayList<>();
					npChunk.add(headWord.valueOf("@id").split("\\.")[1]);
					List<Node> otherNodes = node.selectNodes("W");
					for(Node otherNode: otherNodes) {
//						System.out.println("\t-" + otherNode.getText() + " id = "
//								+ otherNode.valueOf("@id").split("\\.")[1]);
						npChunk.add(otherNode.valueOf("@id").split("\\.")[1]);
					}
					output.add(npChunk);
				}
			}
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		return output;
	}
}
